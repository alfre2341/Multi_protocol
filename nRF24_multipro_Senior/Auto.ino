#define HOVER_MIN 20
#define HOVER_MAX 50
#define LAND 1060

int previousDistance = 0;

void flight() {
  uint32_t timeout;
  int flightStage = 0;
  ppm[0] = 1020;
  ppm[1] = 1500;
  ppm[2] = 1500;
  ppm[3] = 1500;

  lastmeasurement_time = millis();
  Serial.println("Stage 0 BEGIN");

  while (flightStage < 3) {
    timeout = process_SymaX();

    int packetSize = Udp.parsePacket();
    if (packetSize) {
      checkPacket();
      if(stopFlight) {
        flightStage = 2;
        Serial.println("Stage 2 BEGIN");
        previousDistance = altitude();
      }
    }

    if (flightStage == 0) {
      hover();
    }

    if (flightStage == 2) {
      flightStage = land();
      }
    while (micros() < timeout);

  }
}

/*int takeoff() {
  if (millis() - lastmeasurement_time > 3000) {
    lastmeasurement_time = millis();
    Serial.println("Stage 0 COMPLETE");
    Serial.println("Stage 1 BEGIN");
    return 1;
  }
  return 0;
}*/

int land() {
  int distance = altitude();
  if (distance <= 5) {
    ppm[0] = LAND;
    stopFlight = false;
    return 3;
  } else {
    if (millis() - lastmeasurement_time > 500) {
    lastmeasurement_time = millis();
    if (distance <= previousDistance) {
      previousDistance = distance;
    } else if (distance > previousDistance) {
      ppm[0] = ppm[0] - 10;
    }
    if (ppm[0] > PPM_MAX) {
      ppm[0] = PPM_MAX;
    }

    if (ppm[0] < 1020) {
      ppm[0] = 1020;
    }
  }
    return 2;
  }
}

int forwardFlight() {
  if (millis() - lastmeasurement_time > 3000) { //value to have drone hover
    ppm[2] = 1500;
    return 2;
  } else {
    ppm[2] = 1500;
    Serial.println("land");
    return 1;
  }
}

void hover() {
  if (millis() - lastmeasurement_time > 200) {
    int distance = altitude();
    lastmeasurement_time = millis();
    if (distance < HOVER_MIN) {
      ppm[0] = ppm[0] + 10;
    } else if (distance > HOVER_MAX) {
      ppm[0] = ppm[0] - 5;
    }
    if (ppm[0] > PPM_MAX) {
      ppm[0] = PPM_MAX;
    }

    if (ppm[0] < 1020) {
      ppm[0] = 1020;
    }
  }
}

int altitude() {
  long duration;
  int distance;
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  //Distance in cm
  distance = duration * 0.034 / 2;
  return distance;
}

